# 利用正则 爬取豆瓣top250电影信息及图片
# author:xyislove
# 欢迎各位大神修改指导
import requests
import re
from fake_useragent import UserAgent
import csv
import codecs #修正写入csv乱码问题
import os
# 创建图片目录
def mkdir():
    path='./pic'

    isexist=os.path.exists(path)

    if not isexist:
        os.mkdir(path)

    
# 获取网页数据
def get_source(url):
    ua=UserAgent()
    headers={'User-Agent':ua.random}
    resp=requests.get(url,headers=headers)
    # resp.decode()
    
    return resp

# 分析网页
def analyse_page(resp):
    pagecontent=resp.text
    obj=re.compile(r'<li>.*?src="(?P<link>.*?)" class=.*?<span class="title">'
                r'(?P<name>.*?)</span>.*?<br>(?P<year>.*?)&nbsp'
                r'.*?<span class="inq">(?P<appraise>.*?)</span>',re.S)


    result=obj.finditer(pagecontent)
    data_list=[]
    for i in result:
        # print(i.group('name'))
        # Python strip() 方法用于移除字符串头尾指定的字符（默认为空格或换行符）或字符序列。
        
        # print(i.group('year').strip())
        # print(i.group('appraise'))
        # print(i.group('link'))

        dict=i.groupdict()
        dict['year']=i.group('year').strip()
        # print(dict)
        data_list.append(dict)

        # print(type(name))
    return data_list
# 写入数据
def write_data(data_list):
    f=codecs.open('data.csv',mode='a+',encoding='gbk') 
    writer=csv.writer(f)
    for i in data_list:
        writer.writerow(i.values())

# 下载图片

def download_pic(data_list):
    for i in data_list:
        print(i['link'])
        pic=get_source(i['link']).content
        name=i['name']

        with open(f'./pic/{name}.jpg','ab+') as f:
            f.write(pic)
        

if __name__=='__main__':
    mkdir()
    # url='https://movie.douban.com/top250'
    for i in range(0,250,25):
        print(i)
        url=f'https://movie.douban.com/top250?start={i}&filter='
        print(url)
        resp=get_source(url)
        
        
        # print(resp.headers['content-type'])
        data_list=analyse_page(resp)
        write_data(data_list)
        download_pic(data_list)
        # print(data_list)
# Python open() 函数用于打开一个文件，并返回文件对象，在对文件进行处理过程都需要使用到这个函数
# 打开一个文件只用于写入。如果该文件已存在则打开文件，并从开头开始编辑，即原有内容会被删除。
# 如果该文件不存在，创建新文件。
# print(resp.content)
    # resp.encoding
    # print(resp.headers)
    


    

    






